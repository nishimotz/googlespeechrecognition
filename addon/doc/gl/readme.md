# Google Speech Recognition #

* Autores: Mesar Hameed, Beqa Gozalishvili, Noelia Ruiz Martínez,
  colaboradores do NVDA.
* Descarga: [versión 1.0-dev][1]

Este complemento permíteche utilizar o recoñecemento de fala para escreber
texto con só premer unhas teclas.

## Utilización ##

* Abre o cadro de diálogo de configuración deste complemento no menú NVDA.
* Da longa lista de linguas, selecciona as linguas que falas, e preme
  aceptar.
* Polo menos unha lingua ten que estar seleccionada en todo momento.
* Por exemplo Inglés, francés e alemán.
* Agora xa estás listo para usar o recoñecemento de fala, vay a calquer
  campo de texto, como no navegador ou un documento de texto.
* Preme NVDA+y para cambiar entre as linguas configuradas.
* Para comezar a dictar, preme NVDA+shift+g, escoitarás un pitido de ton
  alto.
* Comeza a falar na lingua escollida.
* Para deter o dictado, preme o mesmo atallo de teclado, e nesta vez escoitaráse un pitido grave.
Nota: se non paras a grabación, deterase automáticamente despois de 10 segundos.
Tense en conta o intervalo de tempo máis curto, para ter a precisión máis alta.
* Ha voz enviarase ó servicio de recoñecemento de fala de Google, e cando se devolva a transcripción, NVDA falaráa.
* Se o recoñecemento é correcto, preme o atallo de teclado para aceptar, NVDA+control+shift+g, e o texto escribiráse á posición do cursor.
* Se o recoñecemento non foi correcto, simplemente graba pero utiliza un anaco máis pequeno
de palabras. Pode que sexa necesario dividir unha frase longa en dous ou tres anacos.
* Cando queiras falar nunha lingua diferente, simplemente cambia a unha das túas linguas e repite os pasos.

## Ordes de Teclado ##

* NVDA+shift+g, Comeza/detén a grabación.
* NVDA+control+shift+g, acepta o resultado da transcripción.
* NVDA+y, cambia entre as túas linguas escollidas.

[[!tag dev]]

[1]: http://addons.nvda-project.org/files/get.php?file=gsr-dev
