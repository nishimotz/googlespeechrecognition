# Google Speech Recognition #

* Auteurs: Mesar Hameed, Beqa Gozalishvili, Noelia Ruiz Martínez,
  contributeurs de NVDA.
* Télécharger [version 1.0-dev][1]

Ce module complémentaire vous permet d'utiliser la reconnaissance vocale
pour écrire du texte à l'aide de quelques touches de raccourci.

## Utilisation ##

* Ouvrez la boîte de dialogue de configuration de ce module compplémentaire
  à partir du menu NVDA.
* Dans la longue liste de langues, sélectionnez les langues que vous parlez,
  et appuyez sur Accepter.
* Au moins une langue doit être sélectionnée à tout moment.
* Par exemple anglais, français et allemand.
* Maintenant vous êtes prêt à utiliser la reconnaissance vocale, aller à
  n'importe quel champ de texte comme dans votre navigateur ou à un document
  texte.
* Appuyez sur NVDA + y pour basculer entre vos langues configurées.
* Pour commencer à dicter, appuyez sur NVDA + Maj + G, vous entendrez un bip
  très 
* Commencer à parler dans la langue choisie.
* Pour arrêter la dictée, appuyez sur le même raccourci, et cette fois, vous entendrez un bip moins nRemarque : Si vous n'arrêtez pas l'enregistrement, il sera automatiquement arrêté après 10 secondes.
Moins l'interval est grand, plus fiable sera le résultat.
* L'allocution sera envoyé au service de reconnaissance vocale de Google, et quand la transcription est retourné, NVDA annoncera le résultat.
* Si la reconnaissance est correcte, appuyez sur le raccourci accepter, NVDA + control + shift + g, et le texte sera écrit à la position du curseur.
* Si la reconnaissance n'est pas correct, il suffit de réenregistrer un morceau plus petit
de phrase. Vous pourriez avoir besoin de diviser une longue phrase en deux ou trois morceaux.
* Lorsque vous voulez parler dans une langue différente, choisissez simplement la  langues et répétez les étapes.

## Touches de Raccourci ##

* NVDA + Maj + G, Démarrer / arrêter l'enregistrement.
* NVDA + control + shift + g, accepter le résultat de la transcription.
* NVDA + y, cycle entre vos langues choisies.

[[!tag dev]]

[1]: http://addons.nvda-project.org/files/get.php?file=gsr-dev
