# Googlen puheentunnistus #

* Tekijät: Mesar Hameed, Beqa Gozalishvili, Noelia Ruiz Martínez, NVDA:n
  tekijät.
* Lataa [versio 1.0-dev][1]

Tämän lisäosan avulla voit käyttää puheentunnistusta tekstin kirjoittamiseen
muutamia näppäinkomentoja käyttäen.

## Käyttö ##

* Avaa asetusvalintaikkuna NVDA-valikosta.
* Valitse tunnistettavat kielet kielten listasta ja paina OK.
* Koko ajan on oltava valittuna vähintään yksi kieli.
* Esim. englanti, saksa ja ranska.
* Voit nyt käyttää puheentunnistusta menemällä mihin tahansa tekstikenttään,
  kuten esim. selaimessa, tai tekstiasiakirjaan.
* Vaihda määritettyjen kielten välillä painamalla NVDA+Y.
* Aloita sanelu painamalla NVDA+Shift+G, jonka jälkeen kuuluu korkea
  äänimerkki.
* Ala puhumaan valitulla kielellä.
* Keskeytä sanelu painamalla samaa näppäinkomentoa, jonka jälkeen kuuluu matala äänimerkki.
Huomautus: Mikäli et keskeytä äänitystä, se lopetetaan automaattisesti 10 sekunnin jälkeen.
Kannattaa pitää mielessä, että tunnistus on sitä tarkempi, mitä lyhempi äänite on.
* Äänite lähetetään Googlen puheentunnistuspalveluun. Kun tunnistuksen tulos palautetaan, NVDA lukee sen.
* Mikäli tunnistus on oikein, paina hyväksymispikanäppäintä, NVDA+Ctrl+Shift+G, jonka jälkeen teksti lisätään nykyiseen kohdistimen sijaintiin.
* Mikäli tunnistus oli virheellinen, äänitä yksinkertaisesti uudelleen
pienemmällä sanamäärällä. Pitkän lauseen jakaminen kahteen tai kolmeen osaan saattaa olla tarpeen.
* Kun haluat käyttää eri kieltä, vaihda johonkin valituista kielistä ja toista nämä vaiheet.

## Näppäinkomennot ##

* NVDA+Shift+G: Aloita/keskeytä äänitys.
* NVDA+Control+Shift+G: Hyväksy tunnistuksen tulos.
* NVDA+Y: Vaihda valittujen kielten välillä.

[[!tag dev]]

[1]: http://addons.nvda-project.org/files/get.php?file=gsr-dev
