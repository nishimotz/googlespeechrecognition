# Reconhecimento de voz do Google #

* Autores: Mesar Hameed, Beqa Gozalishvili, Noelia Ruiz Martínez,
  colaboradores do NVDA.
* Baixe a [versão 1.0-dev][1]

Este complemento habilita você a usar reconhecimento de voz para escrever
texto com umas poucas teclas de atalho.

## Uso ##

* Abra a janela de opções deste complemento a partir do menu do NVDA.
* Na longa lista de idiomas, selecione aquele que você fala e pressione OK.
* Pelo menos um idioma tem de estar selecionado a qualquer momento.
* Por exemplo Inglês, Francês e Alemão.
* Agora você está pronto para usar o reconhecimento de voz; vá para um campo
  de texto, como algum no navegador ou um documento de texto.
* Pressione NVDA+y para circular entre os idiomas que configurou.
* Para começar a ditar, pressione NVDA+shift+g; ouvirá um bipe agudo.
* Comece a falar no idioma escolhido.
* Para interromper o ditado, pressione a mesma tecla; desta vez ouvirá um bipe grave.
Nota: Caso não interrompa a gravação, esta será automaticamente interrompida após 10 segundos.
Tenha em mente que quanto mais curto o tempo, mais exato é o reconhecimento.
* A fala será transmitida ao serviço de reconhecimento de voz do Google, e quando vier a transcrição, o NVDA anunciará a mesma.
* Caso o reconhecimento esteja correto, pressione a tecla de aceitar, NVDA+control+shift+g, e o texto será escrito na posição do cursor.
* Se o reconhecimento não ficou correto, simplesmente regrave, mas use um trecho menor
de palavras. Talvez precise quebrar uma sentença em dois ou três trechos.
* Quando quiser falar noutro idioma, simplesmente alterne para um dos outros e repita os passos.

## Teclas de comando ##

* NVDA+shift+g, começa/pára de gravar.
* NVDA+control+shift+g, aceita o resultado da transcrição.
* NVDA+y, circula entre os idiomas escolhidos.

[[!tag dev]]

[1]: http://addons.nvda-project.org/files/get.php?file=gsr-dev
