# Google Beszéd-felismerő #

* Készítők: Mesar Hameed, Beqa Gozalishvili, Noelia Ruiz Martínez, NVDA
  közreműködők.
* Letöltés [version 1.0-dev][1]

A kiegészítő segítségével használható a beszédfelismerő szöveg gépelésre,
néhány billentyűparancs megnyomásával

## Használat ##

* Megnyitja a konfigurációs mappáját a kiegészítőnek az NVDA menüből.
* A hosszú listából válassza ki azokat a nyelveket, amelyeken beszél, és
  nyomja meg az ok gombot.
* Minden alkalommal legalább egy nyelvet kell kiválasztani.
* Pl. Angol, francia és német.
* Mostantól használatba veheti a szöveg felismerőt, csak keressen egy szöveg
  bevitelére alkalmas mezőt, pl. a böngészőjében vagy egy szöveges
  dokumentumban.
* Nyomja meg az NVDA+y billentyűparancsot a beállított nyelvek közötti
  váltáshoz.
* A diktálás megkezdéséhez nyomja meg az NVDA+shift+g billentyűparancsot,
  ekkor egy magas csippanást fog hallani.
* Kezdjen el beszélni a kiválasztott nyelven.
A diktálás leállításához nyomja meg ugyanezt a parancsot, ekkor egy mélyebb csippanást fog hallani.
Megjegyzés: Ha nem állítja le a felvételt, tíz másodperc után ez megtörténik automatikusan.
A rövidebb intervallum nagyobb pontosságot eredményez.
*A beszéd elküldésre kerül a Google beszédfelismerő szolgáltatásának, majd amikor felismertetésre került, az NVDA bemondja azt.
Ha elégedett a szöveg minőségével, nyomja meg az NVDA+shift+control+g-t, ekkor a szöveg beillesztésre kerül a kurzor pozíciójához.
Ha nem elégedett a kapott szöveggel, vágja szét kisebb darabokra.
* Ha más nyelven szeretne felismertetést végezni, csak váltson a hozzáadott nyelvek között, és ismételje meg ezeket a lépéseket.

## Billentyűparancsok ##

* NVDA+shift+g, a felvétel elindítása/leállítása
* NVDA+control+shift+g, a gépelés elfogadása
* NVDA+y, váltás a kiválasztott nyelvek között

[[!tag dev]]

[1]: http://addons.nvda-project.org/files/get.php?file=gsr-dev
