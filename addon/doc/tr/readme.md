# Google Konuşma Tanıma #

* Geliştirenler: Mesar Hameed, Beqa Gozalishvili, Noelia Ruiz Martínez, NVDA
  katkıda bulunanlar.
* İndir [version 1.0-dev][1]

Bu eklenti birkaç kısayol tuşuna basmak suretiyle konuşma tanıma yoluyla
yazmanızı sağlar.

## Kullanım ##

* NVDA menüsünden Bu eklenti için konfigürasyon iletişim kutusunu açın.
* Uzun dil listesinden konuştuğunuz dilleiri seçin ve tamam tuşuna basın.
* En az bir dil seçilmelidir.
* Örneğin İngilizce, Fransızca ve Türkçe.
* Şimdi konuşma tanımayı kullanmaya hazırsın, Herhangi bir yazı alanına
  gidin ya da bir belge açın.
* Belirlediğiniz diller arasından birini seçmek için NVDA+y tuşlarını
  kullanın.
* Dikte etmeye başlamak için NVDA+shift+g tuşlarına basın, ince bir bip sesi
  duyacaksınız. 
* Seçtiğiniz dilde konuşun.
* Dikte etmeyi durdurmak için, aynı kısayola basın, ve bu kez daha kalın bir bip sesi duyacaksınız.
Not: Eğer kaydı durdurmazsanız, 10 saniye sonra otomatik olarak duracaktır.
Kısa kayıtlar için doğru tanıma oranının daha yüksek olacağını aklınızdan çıkarmayın.
* Konuşma kaydı google konuşma tanıma servisine gönderilecek, ve sonuç geldiğinde, NVDA seslendirecektir.
* Eğer sonuç doğruysa, NVDA+control+shift+g tuşlarına basarak kabul edin, ve metin imlecin bulunduğu yere yapıştırılsın.
* Tanıma doğru değilse, daha kısa kayıtlar yapmayı deneyin.
Bir cümleyi birden fazla bölümler halinde kaydetmeniz gerekebilir.
* Başka bir dilde konuşma tanıma için, NVDA+y ile dili seçip aynı adımları tekrarlayın.

## Kısayol Komutları ##

* NVDA+shift+g, kaydı başlatıp durdur.
* NVDA+control+shift+g, Tanıma sonucunu onayla.
* NVDA+y, belirlediğiniz diller arasından seçim yapın.

[[!tag dev]]

[1]: http://addons.nvda-project.org/files/get.php?file=gsr-dev
