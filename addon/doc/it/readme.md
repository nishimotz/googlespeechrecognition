# Google Speech Recognition #

* Autori: Mesar Hameed, Beqa Gozalishvili, Noelia Ruiz Martínez,
  Collaboratori di NVDA.
* Download [versione 1.0-dev][1]

Questo componente aggiuntivo permette di utilizzare il riconoscimento vocale
per scrivere del testo con pochissimi tasti.

## Utilizzo ##

* Aprire la finestra di configurazione per questo componente aggiuntivo dal
  menu NVDA.
* Dall'elenco molto lungo di lingue, selezionare quelle che si parlano e
  premere ok.
* Almeno una lingua deve essere selezionata in ogni momento.
* Ad esempio inglese, francese e tedesco.
* Ora si è pronti per utilizzare il riconoscimento vocale, andare in un
  qualsiasi campo di testo come ad esempio nel browser o in un documento di
  testo.
* Premere NVDA+y per passare da una lingua configurata all'altra.
* Per iniziare a dettare, premere NVDA + shift + g, si sentirà un segnale
  acustico alto e acuto.
* Iniziare a parlare nella lingua scelta.
* To stop dictation, press the same shortcut, and this time you will hear a low pitched beep.
Note: if you do not stop the recording, it will automatically be stopped after 10 seconds.
Bare in mind the shorter time interval, the higher accuracy.
* The speech will be sent to the Google speech recognition service, and when the transcription is returned, NVDA will announce it.
* If the recognition is correct, press the accept shortcut, NVDA+control+shift+g, and the text will be written to your cursor position.
* If the recognition was not correct, simply rerecord but use a smaller chunk
of words. You might need to split a long sentence into two or three chunks.
* When you want to speak in a different language, simply cycle to one of your languages and repeat the steps.

## Tasti rapidi ##

* NVDA + shift + g, avvia/arresta la registrazione.
* NVDA + control + shift + g, accetta il risultato del riconoscimento.
* NVDA+y, Cycle between your chosen languages.

[[!tag dev]]

[1]: http://addons.nvda-project.org/files/get.php?file=gsr-dev
