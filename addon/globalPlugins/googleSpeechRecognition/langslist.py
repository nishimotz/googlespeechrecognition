# encoding: utf-8

# Speech Recognition powered by Google
# A global plugin for NVDA
# Copyright 2013 NVDA contributors, released under GPL.
# See copying.txt for license information.

import collections
from languageHandler import getLanguageDescription

# To find out what languages need to be marked for translation, which windows dont automatically translate for us
# uncomment some of the dialects and reload the addon.
# Note that different versions of windows have different lists, so best to try with windows xp.
# Then replace the call getLanguageDescription by the _("formal name")
#
langsList = {
	"af": getLanguageDescription('af'),
	"ar": getLanguageDescription('ar'),
	#"ar_AE": getLanguageDescription('ar_AE'),
	#"ar_BH": getLanguageDescription('ar_BH'),
	#"ar_DZ": getLanguageDescription('ar_DZ'),
	#"ar_EG": getLanguageDescription('ar_EG'),
	#"ar_IQ": getLanguageDescription('ar_IQ'),
	#"ar_JO": getLanguageDescription('ar_JO'),
	#"ar_KW": getLanguageDescription('ar_KW'),
	#"ar_LB": getLanguageDescription('ar_LB'),
	#"ar_LY": getLanguageDescription('ar_LY'),
	#"ar_MA": getLanguageDescription('ar_MA'),
	#"ar_OM": getLanguageDescription('ar_OM'),
	#"ar_QA": getLanguageDescription('ar_QA'),
	#"ar_SA": getLanguageDescription('ar_SA'),
	#"ar_TN": getLanguageDescription('ar_TN'),
	#"ar_YE": getLanguageDescription('ar_YE'),
	"bg": getLanguageDescription('bg'),
	"ca": getLanguageDescription('ca'),
	"cs": getLanguageDescription('cs'),
	"de": getLanguageDescription('de'),
	#"en_AU": getLanguageDescription('en_AU'),
	#"en_CA": getLanguageDescription('en_CA'),
	#"en_IN": getLanguageDescription('en_IN'),
	#"en_NZ": getLanguageDescription('en_NZ'),
	#"en_ZA": getLanguageDescription('en_ZA'),
	"en_GB": getLanguageDescription('en_GB'),
	"en_US": getLanguageDescription('en_US'),
	"es": getLanguageDescription('es'),
	#"es_AR": getLanguageDescription('es_AR'),
	#"es_BO": getLanguageDescription('es_BO'),
	#"es_CL": getLanguageDescription('es_CL'),
	#"es_CO": getLanguageDescription('es_CO'),
	#"es_CR": getLanguageDescription('es_CR'),
	#"es_DO": getLanguageDescription('es_DO'),
	#"es_EC": getLanguageDescription('es_EC'),
	#"es_SV": getLanguageDescription('es_SV'),
	#"es_GT": getLanguageDescription('es_GT'),
	#"es_HN": getLanguageDescription('es_HN'),
	#"es_MX": getLanguageDescription('es_MX'),
	#"es_NI": getLanguageDescription('es_NI'),
	#"es_PA": getLanguageDescription('es_PA'),
	#"es_PY": getLanguageDescription('es_PY'),
	#"es_PE": getLanguageDescription('es_PE'),
	#"es_PR": getLanguageDescription('es_PR'),
	#"es_ES": getLanguageDescription('es_ES'),
	#"es_US": getLanguageDescription('es_US'),
	#"es_UY": getLanguageDescription('es_UY'),
	#"es_VE": getLanguageDescription('es_VE'),
	"eu": getLanguageDescription('eu'),
	"fi": getLanguageDescription('fi'),
	"fr": getLanguageDescription('fr'),
	"gl": getLanguageDescription('gl'),
	"he": getLanguageDescription('he'),
	"hu": getLanguageDescription('hu'),
	"id": getLanguageDescription('id'),
	"is": getLanguageDescription('is'),
	"it": getLanguageDescription('it'),
	"ja": getLanguageDescription('ja'),
	"ko": getLanguageDescription('ko'),
	"ms": getLanguageDescription('ms'),
	"nl": getLanguageDescription('nl'),
	"no": getLanguageDescription('no'),
	"pl": getLanguageDescription('pl'),
	"pt_BR": getLanguageDescription('pt_BR'),
	"pt_PT": getLanguageDescription('pt_PT'),
	"ro": getLanguageDescription('ro'),
	"ru": getLanguageDescription('ru'),
	"sk": getLanguageDescription('sk'),
	"sr": getLanguageDescription('sr'),
	"sv": getLanguageDescription('sv'),
	"tr": getLanguageDescription('tr'),
	"zh_CN": getLanguageDescription('zh_CN'),
	"zh_TW": getLanguageDescription('zh_TW'),
	"zh_HK": getLanguageDescription('zh_HK'),
}

orderedLangsList = collections.OrderedDict(sorted(langsList.items(), key=lambda t: t[0]))
langsCodes = orderedLangsList.keys()

